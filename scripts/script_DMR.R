# author: "Pauline PORTE"
# date: "2022-12-05"

#############################################################

# Libraries utilisées
library(limma)
library(tidyverse)

#############################################################

# Metadata
metadata <- read.csv("Data/metadata.csv", row.names=1)
metadata$sample <- as.vector(metadata$sample)
metadata <- metadata[order(metadata$sample),]

metadata_IDHmut <- metadata[c(1:3, 8:10, 14:15),]
metadata_IDHwt <- metadata[c(1:7, 11:13, 16),]

#############################################################

# Préparation des données méthylomiques
options(scipen = 100, digits = 4)
HM450k <- read.delim("Data/HM450k/HM450k_filtered_norm_443691CpG.txt", header=TRUE)
HM450k <- na.omit(HM450k)
HM450k <- as.data.frame(HM450k[,c(1,4:19)]) 
rownames(HM450k) <- HM450k[,1] 
HM450k <- HM450k %>% select(!c(1))

#############################################################

# DMR IDH avec Limma
metadata$IDH <- factor(metadata$IDH)
metadata$IDH <- relevel(metadata$IDH, ref="Control")

design_nsolver <- model.matrix(~ IDH, data = metadata)
colnames(design_nsolver)

#fit_nsolver <- voom(HM450k, design_nsolver) 
fit_nsolver <- limma::lmFit(HM450k, design_nsolver)
fit_nsolver <- limma::eBayes(fit_nsolver, robust = T)

DMR_HM450k <- limma::topTable(fit_nsolver, n=Inf)

DMR_HM450k_filtered <- DMR_HM450k %>% filter(adj.P.Val < 0.05) %>% rownames_to_column("ID")

DMR_IDH_list <- DMR_HM450k_filtered$ID 
write(DMR_IDH_list, "Data/DMR/DMR_IDH_list.txt")
