import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

######
#On ouvre un csv qui contient des entètes, en ligne les individus, l'expression des gènes en colonnes. 
######

df1 = pd.read_csv('Data/RNA_Seq/RNAseq_GSE123892_counts_gencode_V19_2_court.txt', 
		   sep='\t', 
		   index_col=0)
df = df1.T

print("Tableau de données de départ :")
print(df.head())
print()

######
#On produit un array avec pour chaque ligne, la liste des valeurs d'expression de chacun des gènes : 
######

#Dans l'exemple :

#Dans notre cas (on sélectionne toutes les colonnes car tous les gènes) : 
X = df.iloc[:,].values
print(X[:10])

######
#Déterminer le nombre de clusters avec la méthode Elbow
######

#On lance l'algo avec 1,2,...10 et on visualise pour déterminer le nombre de clusters optimal à utiliser. 
#On part sur une initialisation au hasard des centroïdes (init = 'random' et randomstates)

'''
clustering_score = []
for i in range(1, 10):
    kmeans = KMeans(n_clusters = i, init = 'random', random_state = 42)
    kmeans.fit(X)
    clustering_score.append(kmeans.inertia_)

plt.figure(figsize=(10,6))
plt.plot(range(1, 10), clustering_score)
plt.title('Elbow Method on data')
plt.xlabel('No. of Clusters')
plt.ylabel('Clustering Score')
plt.show()
'''

######
#Clustering Kmeans et visualisation
######

#On compute le clustering
kmeans= KMeans(n_clusters = 4, random_state = 42)
kmeans.fit(X)

#On stock dans quel cluster sont chacun des échantillons
pred = kmeans.predict(X)

#On ajoute une colonne à la dataframe pour voir dans quel cluster est placé chaque échantillon
df['Cluster'] = list(pred)

#Visualisation rapide du nombre de membre de chaque cluster
print('Number of data points in each cluster : \n', df['Cluster'].value_counts())

#Visualisation de la table et des cluster associés à chaque échantillons
print(df)


exit()

plt.figure(figsize=(10,6))
plt.scatter(X[pred == 0, 0], X[pred == 0, 1], c = 'brown', label = 'Cluster 0')
plt.scatter(X[pred == 1, 0], X[pred == 1, 1], c = 'green', label = 'Cluster 1')
#plt.scatter(X[pred == 2, 0], X[pred == 2, 1], c = 'blue', label = 'Cluster 2')
#plt.scatter(X[pred == 3, 0], X[pred == 3, 1], c = 'purple', label = 'Cluster 3')
#plt.scatter(X[pred == 4, 0], X[pred == 4, 1], c = 'orange', label = 'Cluster 4')

plt.scatter(kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:, 1],s = 300, c = 'red', label = 'Centroid', marker='*')

plt.legend()
plt.title('Customer Clusters')
plt.show()
























