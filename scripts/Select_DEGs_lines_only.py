IDs_IDH = list()
IDs_grades = list()

with open ("Data/DEGs/DEG_IDH_list.txt","r") as f1 :
	for l in f1 :
		IDs_IDH.append(l.strip())
with open ("Data/DEGs/DEG_grades_list.txt","r") as f1 :
	for l in f1 :
		IDs_grades.append(l.strip())
		

with open ("Data/count_IDH_DEGs.tsv","w") as out :
	with open ("Data/RNA_Seq/RNAseq_GSE123892_counts_gencode_V19_2_court.txt","r") as f1 :
		for l in f1 : 
			if l.strip().split("\t")[0] in IDs_IDH :
				print(l.strip(), file=out)				
del IDs_IDH
with open ("Data/count_IDH_Grades.tsv","w") as out2 :
	with open ("Data/RNA_Seq/RNAseq_GSE123892_counts_gencode_V19_2_court.txt","r") as f1 :
		for l in f1 : 
			if l.strip().split("\t")[0] in IDs_grades :
				print(l.strip(), file=out2)				
