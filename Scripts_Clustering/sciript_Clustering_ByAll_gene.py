import pandas as pd
import numpy as np
import seaborn as sns
from PIL import Image
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from scipy.spatial.distance import squareform


# Load data : Path to data
df = pd.read_csv("/home/hm/PycharmProjects/pythonProject3/Omics_intgrtv_/RNAseq_GSE123892_counts_gencode_V19_2_court.txt",sep="\t")
#Rename first column :
df.rename(columns={'Unnamed: 0': 'ID-gene'},inplace=True, errors='raise')
# Now we delete a column that has at least 16 missing value = we have 16 class of individual
print("Data befor cleanig",df.shape)
df.iloc[:,1].dropna(how ='any')
print("Data after cleanig",df.shape)


##### Step 1 : correlation matrix : method = Pearson

df=df.iloc[:,1:] # without first columns.
plt.figure(figsize=(15,10))
correlations = df.corr()
sns.heatmap(round(correlations,2), cmap='RdBu', annot=True, annot_kws={"size": 7}, vmin=-1, vmax=1)

plt.title("Heatmap correlation of individuals using gene expression  ",fontsize = 22)
plt.savefig('Heatmap_Corr_Patient.pdf',dpi=300,)
plt.show()


##### Step 2 : clustering

from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from scipy.spatial.distance import squareform
plt.figure(figsize=(25,15))

dissimilarity = 1-abs(correlations)
Z = linkage(squareform(dissimilarity),'average') #complete
dendrogram(Z, labels=df.columns, orientation='top', leaf_rotation=90)

plt.title("Patient clustering dendrogram ",fontsize = 22)
plt.xlabel("Patients",fontsize = 15)
plt.ylabel("Distance",fontsize = 15)
plt.savefig('Patient_clustering_dendrogram.pdf',dpi=300)
plt.show()

