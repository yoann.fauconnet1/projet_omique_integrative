import pandas as pd
import numpy as np
import seaborn as sns
from PIL import Image
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from scipy.spatial.distance import squareform

######################################### Clustering on GRADES ###############################################

# Load data : Path to data
df_Grades = pd.read_csv("count_IDH_Grades.tsv",sep="\t")
# Make columns name to dataframe :
columns_indiv=['ID-gene', 'C_1', 'C_4', 'CF_4SB', 'GBM_4', 'GBM_7', 'GBM_27', 'GBM_34', 'GII_2', 'GII_4', 'GII_5', 'GIII_4', 'GIII_11', 'GIII_12', 'GIII_17', 'GIII_22', 'GIII_24']
df_Grades.columns=columns_indiv

# Now we delete a column that has at least 16 missing value = we have 16 class of individual
print("Data befor cleanig",df_Grades.shape)
df_Grades.iloc[:,1].dropna(how ='any')
print("Data after cleanig",df_Grades.shape)


##### Step 1 : correlation matrix : method = Pearson

df_Grades=df_Grades.iloc[:,1:] # without first columns.
plt.figure(figsize=(15,10))
correlations = df_Grades.corr()
sns.heatmap(round(correlations,2), cmap='RdBu', annot=True, annot_kws={"size": 7}, vmin=-1, vmax=1)

plt.title("Heatmap correlation of_Patient-BY-GRADES-using gene expression",fontsize = 22)
plt.savefig('Heatmap_Corr_Patient-BY-GRADES.pdf',dpi=300,) # ------------------
plt.show()


##### Step 2 : clustering

from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from scipy.spatial.distance import squareform
plt.figure(figsize=(25,15))

dissimilarity = 1-abs(correlations)
Z = linkage(squareform(dissimilarity),'average') #complete
dendrogram(Z, labels=df_Grades.columns, orientation='top', leaf_rotation=90)

plt.title("Patient-BY-GRADES-clustering dendrogram ",fontsize = 22)
plt.xlabel("Patients",fontsize = 15)
plt.ylabel("Distance",fontsize = 15)
plt.savefig('Patient-BY-GRADES-clustering_dendrogram.pdf',dpi=300)
plt.show()

##################################### Clustering on IDH situation ############################################

# Load data : Path to data
df_IDH = pd.read_csv("count_IDH_DEGs.tsv",sep="\t")
# Make columns name to dataframe :
columns_indiv=['ID-gene', 'C_1', 'C_4', 'CF_4SB', 'GBM_4', 'GBM_7', 'GBM_27', 'GBM_34', 'GII_2', 'GII_4', 'GII_5', 'GIII_4', 'GIII_11', 'GIII_12', 'GIII_17', 'GIII_22', 'GIII_24']
df_IDH.columns=columns_indiv

# Now we delete a column that has at least 16 missing value = we have 16 class of individual
print("Data befor cleanig",df_IDH.shape)
df_IDH.iloc[:,1].dropna(how ='any')
print("Data after cleanig",df_IDH.shape)


##### Step 1 : correlation matrix : method = Pearson

df_IDH=df_IDH.iloc[:,1:] # without first columns.
plt.figure(figsize=(15,10))
correlations = df_IDH.corr()
sns.heatmap(round(correlations,2), cmap='RdBu', annot=True, annot_kws={"size": 7}, vmin=-1, vmax=1)

plt.title("Heatmap correlation of_Patient-BY-IDH-using gene expression",fontsize = 22)
plt.savefig('Heatmap_Corr_Patient-BY-IDH.pdf',dpi=300,)
plt.show()


##### Step 2 : clustering

from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from scipy.spatial.distance import squareform
plt.figure(figsize=(25,15))

dissimilarity = 1-abs(correlations)
Z = linkage(squareform(dissimilarity),'average') #complete
dendrogram(Z, labels=df_IDH.columns, orientation='top', leaf_rotation=90)

plt.title("Patient-BY-IDH-clustering dendrogram ",fontsize = 22)
plt.xlabel("Patients",fontsize = 15)
plt.ylabel("Distance",fontsize = 15)
plt.savefig('Patient-BY-IDH-clustering_dendrogram.pdf',dpi=300)
plt.show()

