###################################################################################################
# Script : Cette API - Ensembl permet l'annotation pour chaque ID ENS renseigner                  #
# Develloped by Toufik-H                                                                          #
# 05-12-2023                                                                                      #
# ce script est automatisé : en ligne de commande renseigner :                                    #
                            # 1 Le script                                                         #
                            # 2 Fichier (txt) contenant les gene ID en une colonne                #
                            # 3 Fichier de sortie des output                                      #
                            # Exemple : python3 scriptXX DEG_IDH_list.txt DEG_IDH_Inforamtion.txt #
###################################################################################################

### IMPORTATION PACKAGE
import os
import sys
import requests, sys

# le serveur sur lequelle ce lance la requette = ENSEMBL
server = "https://rest.ensembl.org"

with open(sys.argv[1], "r") as f1, open(sys.argv[2], "w") as f2:
    Nb_geneID_base=0
    Nb_geneID_No_trouve = 0
    for line in f1:
        Nb_geneID_base+=1
        line =line.strip("\n").split(".")[:][0] # prétraitement (suppression des num des chrom de chaque ID ENS)

        # lacement des requettes
        # vous pouvez ajouté un ouverlap1 .. pour en extraire plus d'information chauvochantes
        ext = "/phenotype/gene/homo_sapiens/"+str(line)
        r = requests.get(server + ext, headers={"Content-Type": "application/json"})

        # La réponse sur les réquettes
        if not r.ok:
            Nb_geneID_No_trouve+=1
            #r.raise_for_status() # inactivé sin = erreur if ID not found
            #sys.exit()
            absent=("The ID ENS :" ,line, " not found")
        decoded = r.json()
        print(repr(decoded),file=f2)

    # Statistique + infos : A la fin du fichier des output
    print("\n\n- Le nombre d'ID ENS détectés dans la BDD Ensembl suite a la requette :", Nb_geneID_base - Nb_geneID_No_trouve,file=f2)
    print("- Le nombre d'ID ENS Non détectés dans la BDD Ensembl suite a la requette : ", Nb_geneID_No_trouve,"\n\t sont : \n\t\t",absent,file=f2)
    print("- Sachant: Le nombre d'ID ENS utilisés au départ : ",Nb_geneID_base,file=f2)