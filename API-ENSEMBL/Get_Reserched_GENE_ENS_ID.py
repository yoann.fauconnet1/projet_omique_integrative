
# + + + + + + + + + + + + + + +  + + + + + + + + + + + + + + + ++ + + + + + + + + + + + + + + + + + + + + + + + + + + +
###################################   Script pour récupére les GENE ID ENS     ###################################
########################################################################################################################
### Script to find/ match specifique patterns --> pour avoir les ID ENSG pour chaque mot clés
## Importation:
import re

## Parser les fichiers de sortie de l'AP ensembl:

word_pattern = 'ENSG[az0-9]+' # regExp et pattern utilisé pour matcher les GENE ID ENS
dic_GO_Immun = {}

# accé au fichier contenat les identifiants ontologiques EFO et HP soit : des celles immunitaire/systeme immunitaire
with open("Gene_Ontology_Immunity", "r") as file:
    for lines in file:
        lines=lines.strip("\n").split(" ")
        dic_GO_Immun[lines[0]] = lines[1:]

# Boucle sur les mots clée = C-immunt/ou system immunt ... et en valeur = les ID GO de ces cellules immunitaire ..
for key,val in dic_GO_Immun.items():
    for i in val:
        list_ID = []
        IDG_list = []
        # Ouverture du fichier contenant les Infos trouvé par l'API ENSEMBL + un fichier pour enregistrer les ID ENS en relation avec l'immunité
        with open("DEG_IDH_Info1.txt", "r") as f5, open ("ID_IDH_detectionImmunn2.txt","a") as  f6:
            for ligne in f5:
                ligne=ligne.strip("\n").replace("[" ,"").replace("]","")

                # recherche si : un EFO/HP GO de cette C_immunt et dans la ligne d'un GENE ID ENS
                if i in ligne:
                    # si dans la ligne on récupere le GENE ID ENS
                    a = re.findall(word_pattern, ligne, flags=re.IGNORECASE)
                    # On récupere les ID dans une liste toutes on supprimant les duplicant des GENE ID ENS trouvés

                    if a != []:
                        a = set(a)
                        list_ID.append(list(a))
                        IDG_list = [item for sublist in list_ID for item in sublist]
            # résoudre le probléme ou la liste est vide = cas ou aucun mots clés pour l'GENE ID courant .
            if IDG_list == []:
                print("Aucun ID ensembl pour ce mot clé :", i,"\n\n", file=f6)
    
            elif IDG_list != []:
                print("#### Mot clé rechercher ==>  ",i,"\nGene ID ENS \n",file=f6)
    
                for id in IDG_list:
                    print(id,file=f6)
                print("\n\t\t\t   #########################################\n"
                      "\t\t\t   #########################################  \n\n",file=f6)



# + + + + + + + + + + + + + + +  + + + + + + + + + + + + + + + ++ + + + + + + + + + + + + + + + + + + + + + + + + + + +
# + + + + + + + + + + + + + + +  + + + + + + + + + + + + + + + ++ + + + + + + + + + + + + + + + + + + + + + + + + + + +
### Liste Final Des ID : sans inforamtions supplimentaires (filtration et retenir que les ID ENS)
final_list = []
with open("ID_IDH_detectionImmunn2.txt","r") as f7, open("List_ID_Final_Immun_IDH.txt2","w") as f8:
    for line in  f7:
        line=line.strip("\n")
        if line != "" and line.startswith("ENSG"):
            line=line.split(" ")
            final_list.append(line)
    final_list2 = set([item for sublist in final_list for item in sublist]) # set pour supp les suplicant
    for id in final_list2:
        print(id,file=f8)







# + + + + + + + + + + + + + + +  + + + + + + + + + + + + + + + ++ + + + + + + + + + + + + + + + + + + + + + + + + + + +
# + + + + + + + + + + + + + + +  + + + + + + + + + + + + + + + ++ + + + + + + + + + + + + + + + + + + + + + + + + + + +

"""
# SI besoin  --> Script to split Your data (dans mon cas j'ai déviser des fichier en 3 fichiers ..)
### Script to split Data :
# Séparation des données
import numpy as np
import pandas as pd
data = pd.read_table("RNAseq_GSE123892_counts_gencode_V19_2_court.txt")
data1=(data.iloc[46256:57820,0])
data1.to_csv(r'/home/hm/PycharmProjects/pythonProject3/Snakemake/IDH_TT_list5.txt', header=None, index=None)#, mode='a')
"""


# + + + + + + + + + + + + + + +  + + + + + + + + + + + + + + + ++ + + + + + + + + + + + + + + + + + + + + + + + + + + +
# + + + + + + + + + + + + + + +  + + + + + + + + + + + + + + + ++ + + + + + + + + + + + + + + + + + + + + + + + + + + +

"""
# SI besoin  --> fusionner des fichier en un seul pour une analyse rapide : exemple les fichier de sortie de l'API ENSEMBL)
## Merge files :
# Creating a list of filenames
filenames = ['IDH_Info1.txt', 'IDH_Info2.txt',"IDH_Info3.txt","IDH_Info4.txt","IDH_Info5.txt"]
# Open file3 in write mode
with open('Totaal_ID-Gen_détection[1->5].txt', 'w') as outfile:
    # Iterate through list
    for names in filenames:
        # Open each file in read mode
        with open(names) as infile:
            outfile.write(infile.read())
"""






