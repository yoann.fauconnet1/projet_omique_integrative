
DATA:

Les données utilisés dans cette partie concerne:
  Les DEGs IDH et les Gène totaux (RNA expression)
  Ces deux fichiers  sont disponible dans deux répertoire différents:
  
        1) DATA-DEgs-IDH+resultas, contient:
            Un fichier de sortie de l'API ENSEMBL
            un fichier avec les GENE ID sélectionner + leur C-immunitaire Ou ..
            un fichier d'une liste finale des GENE ID ENSEMBL (propre)

        2) DATA-totaux-RNAEXPR+resultas
            Un fichier de sortie de l'API ENSEMBL
            un fichier avec les GENE ID sélectionner + leur C-immunitaire Ou ..
            un fichier d'une liste finale des GENE ID ENSEMBL (propre)


Script :

    1) GET_phenotype_gene_species_gene.py
    Permet l'annotation d'un gène (Ici les DEGs IDH ou les RNA express = Totaux)a partir de la base de donnée ENSEMBL

     2) Get_Reserched_GENE_ENS_ID.py :
    Ce script permet le filtrage des GENE ID ENSEMBL en ce basant sur un fichier contenant des annotation ontologiques des différentes cellules/parties du système immunitaire
    Donc permet d'extraire les GENE ID reliés a l’immunité

    * Donc ces deux script seront exécutés sur les deux fichiers mentionner dans la partie DATA

      3) L’exécution des deux script nous permet d'avoir :
      Des fichier contenant une liste de gène ID ENSEMBL du système immunitaire seront aussi produit dans les deux répertoire mentionner dans la partie DATA

        L’exécution des deux script est expliqué dans les fichier de Script
